package com.example.exampleslinearrelativejava;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

public class PedroActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pedro);
    }
}